const calculationScreen = document.querySelector(".calculation-screen");
const resultScreen = document.querySelector(".result-screen");

let number = "0";           // 입력중인 숫자
let operator = null;        // 선택한 사칙연산
let calculation = false;    // 1회이상 계산을 하였는지
let firstNumber = null;
let secondNumber = null;

const KEY_CODE = {
  number: [ 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110 ],
  operations: [ 106, 107, 109, 111 ],
  enter: [ 13 ],
  clearEntry: [ 8 ],
  allClear: [27]
}

const NAN_TEXT_CONTENT = "정의되지 않은 결과입니다."

function handlePressKeyboard () {
  document.addEventListener('keydown', function(ev){
    const keyCode = ev.keyCode;
    console.log(keyCode + " " + ev.key);
    if (KEY_CODE.number.includes(keyCode)) clickTheNumberButton("number", ev.key);
    else if (KEY_CODE.operations.includes(keyCode)) clickTheOperationsButton("operations", ev.key);
    else if (KEY_CODE.enter.includes(keyCode)) clickTheEqualsButton("equals");
    else if (KEY_CODE.clearEntry.includes(keyCode)) clickTheClearEntryButton("clear-entry");
    else if (KEY_CODE.allClear.includes(keyCode)) clickTheAllClearButton("all-clear")
  });
}

function clickThePercentButton (buttonType) {
  if (buttonType === "percent") {
    number /= 100;
    resultScreen.textContent = number;
  }
}

function clickTheNagateButton (buttonType) {
  if (buttonType === "nagate") {
    if(number !== "0") {
      number = String(-number);
      resultScreen.textContent = number;
    }
  }
}

function clickTheClearEntryButton (buttonType) {
  if (buttonType === "clear-entry") {
    switch (number.includes("-")) {
      case true:
        if (number.length === 2) number = "0";
      case false:
        if (number.length === 1) number = "0";
        else number = number.substring(0, number.length-1);
      default:
        break;
    }
    resultScreen.textContent = number;
  }
}

function clickTheAllClearButton (buttonType) {
  if (buttonType === "all-clear") {
    firstNumber = null;
    secondNumber = null;
    number = "0";
    calculation = false;
    calculationScreen.textContent = "";
    resultScreen.textContent = "0";
  }
}

function clickTheEqualsButton (buttonType) {
  if (buttonType === "equals") {
    switch (operator) {
      case null:
        firstNumber = Number(number);
        calculationScreen.textContent = `${firstNumber} =`;
        break;
      default:
        secondNumber = Number(number);
        calculationScreen.textContent
          = `${firstNumber} ${operator} ${secondNumber} =`;
        if (operator === "+")      firstNumber += secondNumber;
        else if (operator === "-") firstNumber -= secondNumber;
        else if (operator === "×") firstNumber *= secondNumber;
        else if (operator === "÷") firstNumber /= secondNumber;

        const positionOfDot = String(firstNumber).indexOf(".");
        resultScreen.textContent = String(firstNumber).substring(0, positionOfDot+ 5);
        calculation = true;
        number = "0";
        if (!firstNumber) resultScreen.textContent = NAN_TEXT_CONTENT;
        break;
    }
  }
}

function clickTheOperationsButton (buttonType, buttonValue) {
  if (buttonType === "operations") {
    if (calculation) {
      number = firstNumber;
      firstNumber = null;
      secondNumber = null;
      calculation = false;
    }
      if (firstNumber === null) {
        firstNumber = Number(number);
      } else {
        secondNumber = Number(number);
        if (operator === "+")      firstNumber += secondNumber;
        else if (operator === "-") firstNumber -= secondNumber;
        else if (operator === "×") firstNumber *= secondNumber;
        else if (operator === "÷") firstNumber /= secondNumber;
      }

      if (buttonValue === "*")      operator = "×";
      else if (buttonValue === "/") operator = "÷";
      else                          operator = buttonValue;

      calculationScreen.textContent = `${firstNumber} ${operator}`;
      number = "0";
  }
}

function clickTheNumberButton (buttonType, buttonValue) {
  if (buttonType === "number") {
    if (calculation) {
      firstNumber = null;
      secondNumber = null;
      number = "0";
      operator = null;
      calculation = false;
      calculationScreen.textContent = "";
    }
    switch (buttonValue) {
      case "0":
        if (number === "0") number = buttonValue;
        else number += buttonValue;
        break;
      case ".":
        if (!number.includes(".")) number += buttonValue;
        break;
      default:
        if (number === "0") number = buttonValue;
        else number += buttonValue;
        break;
    }
    resultScreen.textContent = number;
  }
}

function handleClickButton (ev) {
  const buttonType = ev.target.dataset.buttonType;
  const buttonValue = ev.target.textContent;
  clickTheNumberButton(buttonType, buttonValue);
  clickTheOperationsButton(buttonType, buttonValue);
  clickTheEqualsButton(buttonType);
  clickTheAllClearButton(buttonType);
  clickTheClearEntryButton(buttonType);
  clickTheNagateButton(buttonType);
  clickThePercentButton(buttonType);

}

function init () {
  document.querySelectorAll("button").forEach(element => {
    element.addEventListener("click", handleClickButton)
  });
  handlePressKeyboard();
}

init();